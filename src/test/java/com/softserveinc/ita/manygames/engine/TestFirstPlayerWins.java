package com.softserveinc.ita.manygames.engine;


import org.junit.Test;

import static com.softserveinc.ita.manygames.engine.RockScissorsPaper.*;
import static org.junit.Assert.*;


public class TestFirstPlayerWins {



    @Test
    public void testFirstPlayerWinsByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Vasya");
        rockScissorsPaper.setSecondPlayer("Galya");
        rockScissorsPaper.makeTurn("Vasya","ROCK");
        rockScissorsPaper.makeTurn("Galya", "SCISSORS");

        assertTrue(firstPlayerWins());
    }

    @Test
    public void testFirstPlayerLosesByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Vasya");
        rockScissorsPaper.setSecondPlayer("Galya");
        rockScissorsPaper.makeTurn("Vasya","SCISSORS");
        rockScissorsPaper.makeTurn("Galya", "ROCK");

        assertFalse(firstPlayerWins());
    }

    @Test
    public void testFirstPlayerDrawsByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Vasya");
        rockScissorsPaper.setSecondPlayer("Galya");
        rockScissorsPaper.makeTurn("Vasya","SCISSORS");
        rockScissorsPaper.makeTurn("Galya", "SCISSORS");

        assertFalse(firstPlayerWins());
    }

}
