package com.softserveinc.ita.manygames.engine;


import org.junit.Test;

import static com.softserveinc.ita.manygames.engine.RockScissorsPaper.*;
import static org.junit.Assert.*;

public class TestHappensDraw {

    @Test
    public void testHappensDrawByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Vasya");
        rockScissorsPaper.setSecondPlayer("Galya");
        rockScissorsPaper.makeTurn("Vasya","PAPER");
        rockScissorsPaper.makeTurn("Galya", "PAPER");

        assertTrue(happensDraw());
    }

    @Test
    public void testHappensDrawByTheRulesWrong() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Galya");
        rockScissorsPaper.setSecondPlayer("Galya");
        rockScissorsPaper.makeTurn("Vasya","PAPER");
        rockScissorsPaper.makeTurn("Galya", "ROCK");

        assertFalse(happensDraw());
    }

    @Test
    public void testHappensDrawByTheRulesWrongWithNull() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Misha");
        rockScissorsPaper.setSecondPlayer("Olya");
        rockScissorsPaper.makeTurn("Vasya", null);
        rockScissorsPaper.makeTurn("Galya", null);

        assertFalse(happensDraw());
    }


}
