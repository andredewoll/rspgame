package com.softserveinc.ita.manygames.engine;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestValidationPlayerName {

    @Test
    public void testValidationWhenPlayerNameIsNull() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = null;

        boolean actual = rockScissorsPaper.validatePlayerName(playerName);

        assertFalse(actual);
    }

    @Test
    public void testValidationWhenPlayerNameIsNotNull() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = "I am not NULL!!!";

        boolean actual = rockScissorsPaper.validatePlayerName(playerName);

        assertTrue(actual);

    }

    @Test
    public void testValidationWhenPlayerNameIsEmptyString() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = "";

        boolean actual =  rockScissorsPaper.validatePlayerName(playerName);

        assertFalse(actual);
    }

    @Test
    public void testValidationWhenPlayerNameIsNotEmptyString() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String playerName = "I am not Empty   ";

        boolean actual =  rockScissorsPaper.validatePlayerName(playerName);

        assertTrue(actual);
    }



}
