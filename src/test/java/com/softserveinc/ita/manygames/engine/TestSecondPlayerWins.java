package com.softserveinc.ita.manygames.engine;

import org.junit.Test;

import static com.softserveinc.ita.manygames.engine.RockScissorsPaper.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class TestSecondPlayerWins {

    @Test
    public void testSecondPlayerWinsByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Petya");
        rockScissorsPaper.setSecondPlayer("Kolya");
        rockScissorsPaper.makeTurn("Petya","ROCK");
        rockScissorsPaper.makeTurn("Kolya", "PAPER");

        assertTrue(secondPlayerWins());
    }

    @Test
    public void testFirstPlayerLosesByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Petya");
        rockScissorsPaper.setSecondPlayer("Kolya");
        rockScissorsPaper.makeTurn("Petya","ROCK");
        rockScissorsPaper.makeTurn("Kolya", "SCISSORS");

        assertFalse(secondPlayerWins());
    }

    @Test
    public void testFirstPlayerDrawsByTheRules() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        rockScissorsPaper.setFirstPlayer("Petya");
        rockScissorsPaper.setSecondPlayer("Kolya");
        rockScissorsPaper.makeTurn("Petya","ROCK");
        rockScissorsPaper.makeTurn("Kolya", "ROCK");

        assertFalse(secondPlayerWins());
    }
}
