package com.softserveinc.ita.manygames.engine;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestValidatonTurnSyntax {



    @Test
    public void testValidationTurnSyntaxWithRockRight() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String rock = "ROCK";

        boolean actual = rockScissorsPaper.validateTurnSyntax(rock);

        assertTrue(actual);
    }

    @Test
    public void testValidationTurnSyntaxWithRockWrong() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String rock = "ROCk";

        boolean actual = rockScissorsPaper.validateTurnSyntax(rock);

        assertFalse(actual);
    }

    @Test
    public void testValidationTurnSyntaxWithScissorsRight() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String rock = "SCISSORS";

        boolean actual = rockScissorsPaper.validateTurnSyntax(rock);

        assertTrue(actual);
    }

    @Test
    public void testValidationTurnSyntaxWithScissorsWrong() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String rock = "sdsf";

        boolean actual = rockScissorsPaper.validateTurnSyntax(rock);

        assertFalse(actual);
    }

    @Test
    public void testValidationTurnSyntaxWithPaperRight() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String rock = "PAPER";

        boolean actual = rockScissorsPaper.validateTurnSyntax(rock);

        assertTrue(actual);
    }

    @Test
    public void testValidationTurnSyntaxWithPaperWrong() {
        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String rock = null;

        boolean actual = rockScissorsPaper.validateTurnSyntax(rock);

        assertFalse(actual);
    }

}
