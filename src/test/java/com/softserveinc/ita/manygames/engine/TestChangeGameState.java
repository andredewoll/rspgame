package com.softserveinc.ita.manygames.engine;


import org.junit.Test;
import static org.junit.Assert.*;



public class TestChangeGameState {

	@Test
	public void testChangeStateWnenStateIsFirstPlayerWin() {
		RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
		rockScissorsPaper.setFirstPlayer("Vasya");
		rockScissorsPaper.setSecondPlayer("Galya");
		rockScissorsPaper.makeTurn("Vasya","ROCK");
		rockScissorsPaper.makeTurn("Galya", "SCISSORS");

		int expected = GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
		int actual = rockScissorsPaper.changeGameState("Vasya", "ROCK");

		assertEquals(expected, actual);
	}

	@Test
	public void testChangeStateWnenStateIsSecondPlayerWin() {
		RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
		rockScissorsPaper.setFirstPlayer("Petya");
		rockScissorsPaper.setSecondPlayer("Kolya");
		rockScissorsPaper.makeTurn("Petya","ROCK");
		rockScissorsPaper.makeTurn("Kolya", "PAPER");

		int expected = GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
		int actual = rockScissorsPaper.changeGameState("Petya", "ROCK");

		assertEquals(expected, actual);
	}

	@Test
	public void testChangeStateWnenStateIsDraw() {
		RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
		rockScissorsPaper.setFirstPlayer("Olya");
		rockScissorsPaper.setSecondPlayer("Kolya");
		rockScissorsPaper.makeTurn("Olya","ROCK");
		rockScissorsPaper.makeTurn("Kolya", "ROCK");

		int expected = GameState.WAIT_FOR_SECOND_PLAYER_TURN;
		int actual = rockScissorsPaper.changeGameState("Kolya", "ROCK");

		assertEquals(expected, actual);
	}


}
