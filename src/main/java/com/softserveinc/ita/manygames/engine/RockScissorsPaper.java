package com.softserveinc.ita.manygames.engine;


import static com.softserveinc.ita.manygames.engine.GameState.*;

public class RockScissorsPaper extends GenericGameEngine{

    /*Additional static variables for using in game logic */
    static final String ROCK = "ROCK";
    static final String SCISSORS = "SCISSORS";
    static final String PAPER = "PAPER";

    static String firstPlayerChoice;
    static String secondPlayerChoice;


    public RockScissorsPaper(Long id) {
        super(id);
    }

    @Override
    protected boolean validateTurnSyntax(String turn) {
        return ROCK.equals(turn) || SCISSORS.equals(turn)
                || PAPER.equals(turn);
    }

    @Override
    protected boolean validateTurnLogic(String turn) {
        /*Method is not implemented, because our turn logic is
        * implemented in changeGameState method*/
        return true;
    }

    @Override
    protected boolean validatePlayerName(String playerName) {
        return !(playerName == null || playerName.trim().isEmpty());
    }

    @Override
    protected int changeGameState(String playerName, String turn) {

        if (gameState == WAIT_FOR_FIRST_PLAYER_TURN) {
            firstPlayerChoice = turn;
            return WAIT_FOR_SECOND_PLAYER_TURN;
        }
        if (gameState == WAIT_FOR_SECOND_PLAYER_TURN) {
            secondPlayerChoice = turn;
        }

        if (firstPlayerWins()) {
            return FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER;
        }

        if (secondPlayerWins()) {
            return FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
        }

        if (happensDraw()) {
            return FINISHED_WITH_A_DRAW;
        }

        else return WAIT_FOR_FIRST_PLAYER_TURN;
    }

    /*Additional static methods for implementing our game logic */
     static boolean firstPlayerWins() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(SCISSORS)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(PAPER)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(ROCK);
    }

     static boolean secondPlayerWins() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(PAPER)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(ROCK)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(SCISSORS);
    }

    static boolean happensDraw() {
        return firstPlayerChoice.equals(ROCK) && secondPlayerChoice.equals(ROCK)
                || firstPlayerChoice.equals(SCISSORS) && secondPlayerChoice.equals(SCISSORS)
                || firstPlayerChoice.equals(PAPER) && secondPlayerChoice.equals(PAPER);
    }

    /*Running our game*/
    public static void main(String[] args) {

        RockScissorsPaper rockScissorsPaper = new RockScissorsPaper(1L);
        String name1 = "Vasya";
        String name2 = "Petya";
        String turn1 = "ROCK";
        String turn2 = "ROCK";
        String turn3 = "PAPER";
        String turn4 = "ROCK";

        rockScissorsPaper.setFirstPlayer(name1);
        rockScissorsPaper.setSecondPlayer(name2);

        while(rockScissorsPaper.isStarted()) {
            rockScissorsPaper.makeTurn(name1, turn1);
            rockScissorsPaper.makeTurn(name2, turn2);
            rockScissorsPaper.makeTurn(name1, turn3);
            rockScissorsPaper.makeTurn(name2, turn4);
            System.out.println(rockScissorsPaper.toString());
        }
    }
}