package com.softserveinc.ita.manygames.engine;


import java.util.Map;

//Test using Mockito as
//GameEngine ge = Mockito.mock(GenericGameEngine.class, Mockito.CALLS_REAL_METHODS);
//then invoke all implemented methods on ge
public abstract class GenericGameEngine implements GameEngine {
    protected String firstPlayerName;
    protected String secondPlayerName;

    protected int gameState = GameState.STARTED;
    protected int resultCode = GameResultCode.OK;

    private final Long id;
    private String theWinner;

    public GenericGameEngine(final Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    abstract protected boolean validateTurnSyntax(String turn);

    abstract protected boolean validateTurnLogic(String turn);

    abstract protected boolean validatePlayerName(String playerName);

    //We need here
    // 1. Make turn on the Board
    // 2. Determine if the winner is defined - change state to this winner
    // 3. If not - determine next player to turn - change or not waiting player
    abstract protected int changeGameState(String playerName, String turn);

    protected boolean validateTurnOrder(String playerName) {
        boolean firstPlayerCorrectOrder = gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                && firstPlayerName.equals(playerName);
        boolean secondPlayerCorrectOrder = gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN
                && secondPlayerName.equals(playerName);
        return firstPlayerCorrectOrder || secondPlayerCorrectOrder;
    }

    @Override
    public boolean setFirstPlayer(String playerName) {
        if (gameState != GameState.WAIT_FOR_FIRST_PLAYER_NAME) {
            resultCode = GameResultCode.BAD_FIRST_PLAYER_ORDER;
            return false;
        }
        if (validatePlayerName(playerName)) {
            firstPlayerName = playerName;
            resultCode = GameResultCode.OK;
            gameState = GameState.WAIT_FOR_SECOND_PLAYER_NAME;
            return true;
        }
        resultCode = GameResultCode.BAD_PLAYER_NAME;
        return false;
    }

    @Override
    public String getFirstPlayer() {
        return firstPlayerName;
    }

    @Override
    public boolean setSecondPlayer(String playerName) {
        if (gameState != GameState.WAIT_FOR_SECOND_PLAYER_NAME) {
            resultCode = GameResultCode.BAD_SECOND_PLAYER_ORDER;
            return false;
        }
        if (validatePlayerName(playerName)) {
            secondPlayerName = playerName;
            resultCode = GameResultCode.OK;
            gameState = GameState.WAIT_FOR_FIRST_PLAYER_TURN;
            return true;
        }
        resultCode = GameResultCode.BAD_PLAYER_NAME;
        return false;
    }

    @Override
    public String getSecondPlayer() {
        return secondPlayerName;
    }

    @Override
    public String getBoard() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean init(String playerName, Map<String, Object> initData) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object getInfo() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean makeTurn(String playerName, String turn) {
        if (!isStarted()) {
            resultCode = GameResultCode.BAD_TURN_FOR_NOT_STARTED_GAME;
            return false;
        }
        if (isFinished()) {
            resultCode = GameResultCode.BAD_TURN_FOR_FINISHED_GAME;
            return false;
        }
        if (!validatePlayerName(playerName)) {
            resultCode = GameResultCode.BAD_PLAYER_NAME;
            return false;
        }
        if (!validateTurnOrder(playerName)) {
            resultCode = GameResultCode.BAD_TURN_ORDER;
            return false;
        }
        if (!validateTurnSyntax(turn)) {
            resultCode = GameResultCode.BAD_TURN_SYNTAX;
            return false;
        }
        if (!validateTurnLogic(turn)) {
            resultCode = GameResultCode.BAD_TURN_LOGIC;
            return false;
        }

        gameState = changeGameState(playerName, turn);


        if (gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER) {
            setTheWinner(firstPlayerName);
        }
        if (gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER) {
            setTheWinner(secondPlayerName);
        }
        if (gameState == GameState.FINISHED_WITH_A_DRAW) {
            gameState = GameState.WAIT_FOR_FIRST_PLAYER_TURN;
        }
        resultCode = GameResultCode.OK;
        return true;
    }

    @Override
    public int getResultCode() {
        return resultCode;
    }

    @Override
    public boolean isFinished() {
        return gameState == GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER
                || gameState == GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER;
    }

    @Override
    public boolean isStarted() {
        return gameState == GameState.WAIT_FOR_FIRST_PLAYER_TURN
                || gameState == GameState.WAIT_FOR_SECOND_PLAYER_TURN;
    }

    @Override
    public String getTheWinner() {
        return theWinner;
    }

    protected void setTheWinner(String playerName) {
        theWinner = playerName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GenericGameEngine other = (GenericGameEngine) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return String.format("%sGame #%s [ %s vs %s ]: state=%s, code=%s, winner=%s",
                this.getClass().getSimpleName(),
                id, firstPlayerName, secondPlayerName, gameState, resultCode, theWinner);
    }

}