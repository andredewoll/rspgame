package com.softserveinc.ita.manygames.engine;


public class GameResultCode {
    public static final int OK = 0;
    public static final int BAD_PLAYER_NAME = 1;
    public static final int BAD_FIRST_PLAYER_ORDER = 2;
    public static final int BAD_SECOND_PLAYER_ORDER = 3;
    public static final int BAD_TURN_ORDER = 4;
    public static final int BAD_TURN_SYNTAX = 5;
    public static final int BAD_TURN_LOGIC = 6;
    public static final int BAD_TURN_FOR_FINISHED_GAME = 7;
    public static final int BAD_TURN_FOR_NOT_STARTED_GAME = 8;
}